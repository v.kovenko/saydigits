#! /usr/bin/php -q

<?php
/**
 * Озвучивание цифр и денежного представления в Asterisk
 */

/**
 * Разбор числа на миллионы, тысячи, сотни и единицы
 *
 * @param $digits; Число, которое необходимо озвучить
 * @return array; Массив [знак, миллионы, тысячи, сотни, копейки]
 */
function Parse($digits)
{
    // знак
    $sign = $digits < 0 ? 'minus.alaw ' : '';

    // миллионы
    $millions = intval($digits / 1000000);

    // тысячи
    $thousand = intval($digits / 1000) - $millions * 1000;

    // сотни
    $hundreds = intval($digits % 1000);

    // копейки
    $units = round($digits - intval($digits), 2) * 100;

    return [
        'sign'     => $sign,
        'millions' => $millions,
        'thousand' => $thousand,
        'hundreds' => $hundreds,
        'units'    => $units,
    ];
}

/**
 * Разбор сотен
 *
 * @param $digits; Число сотен, которое необходимо озвучить
 * @return string; Аудиофайл
 */
function SayX00($digits)
{
    $item = intval($digits / 100);
    $item = $item > 0 ? strval($item).'00.alaw ' : '';

    return $item;
}

/**
 * Разбор десятков
 *
 * @param $digits; Число десятков, которое необходимо озвучить
 * @return string; Аудиофайл
 */
function SayX0($digits)
{
    $item = intval($digits % 100);

    switch (true) {
        case intval($digits / 10 % 10) == 0:
            $item = '';
            break;
        case 11 <= $item && $item <= 19:
            $item = strval($item).'.alaw ';
            break;
        default:
            $item = strval(10 * intval($item % 100 / 10)).'.alaw ';
    }

    return $item;
}

/**
 * Разбор единиц для тысяч
 *
 * @param $digits; Число единиц, которое необходимо озвучить
 * @param $descriptions; Массив аудиофайлов миллион, миллиона, миллионов / тысяча, тысячи, тысяч
 * @return string Аудиофайл
 * @noinspection PhpDuplicateSwitchCaseBodyInspection
 */
function SayXforThousand($digits, $descriptions)
{
    $suffixes = ['', '_n'];
    $suffix = $suffixes[0];
    $description = $descriptions[0];

    $item = intval($digits % 10);

    switch (true) { // тысяча, тысячи, тысяч
        case 11 <= intval($digits % 100) && intval($digits % 100) <= 19:
            $description = $descriptions[2];
            break;
        case (5 <= $item && $item <= 9) || $item == 0:
            $description = $descriptions[2];
            break;
        case 2 <= $item && $item <= 4:
            if ($item == 1 || $item == 2) {
                $suffix = $suffixes[1];
            }

            $description = $descriptions[1];
            break;
        default:
    }

    if ($item == 1 && intval($digits % 100) != 11) {
        $suffix = $suffixes[1];
    }

    switch (true) { // цифры
        case $item == 0:
            if ($digits) {
                $item = $description;
            } else {
                $item = '';
            }

            break;
        case 11 <= intval($digits % 100) && intval($digits % 100) <= 19:
            $item = $description;
            break;
        default:
        $item .= "{$suffix}.alaw {$description}";
    }

    return empty($item) ? '' : "{$item} pause.alaw ";
}

/**
 * Разбор единиц для миллионов и денежного представления
 *
 * @param $digits; Число единиц, которое необходимо озвучить
 * @param $descriptions; Массив аудиофайлов рубль, рубля, рублей
 * @return string; Аудиофайл
 * @noinspection PhpDuplicateSwitchCaseBodyInspection
 */
function SayX($digits, $descriptions)
{
    $description = $descriptions[0];

    $item = intval($digits % 10);

    switch (true) { // рубль, рубля, рублей
        case intval($digits % 100) == 1:
            break;
        case 11 <= intval($digits % 100) && intval($digits % 100) <= 19:
            $description = $descriptions[2];
            break;
        case (5 <= $item && $item <= 9) || $item == 0:
            $description = $descriptions[2];
            break;
        case 2 <= $item && $item <= 4:
            $description = $descriptions[1];
            break;
        default:
    }

    switch (true) { // цифры
        case $item == 0:
            if ($digits) {
                $item = $description;
            } else {
                $item = '';
            }

            break;
        case 11 <= intval($digits % 100) && intval($digits % 100) <= 19:
            $item = $description;
            break;
        default:
            $item .= empty($description) ? '.alaw' : ".alaw {$description}";
    }

    return empty($item) ? '' : "{$item} pause.alaw ";
}

/**
 * Собирает аудиофайлы для озвучивания как число
 *
 * @param $digits; Число, которое необходимо озвучить
 * @return string; Последовательность аудиофайлов
 * @noinspection PhpUndefinedVariableInspection
 */
function SayNumber($digits)
{
    extract(Parse($digits));

    $say = SayX00($millions)
        .SayX0($millions)
        .SayX($millions, ['million.alaw', 'million_n.alaw', 'million_f.alaw'])
        .SayX00($thousand)
        .SayX0($thousand)
        .SayXforThousand($thousand, ['thousand.alaw', 'thousand_n.alaw', 'thousand_f.alaw'])
        .SayX00($hundreds)
        .SayX0($hundreds)
        .SayX($hundreds, ['', '', '']);
    return trim(empty($say) ? '0.alaw pause.alaw' : $say);
}

/**
 * Собирает аудиофайлы для озвучивания в денежном представлении
 *
 * @param $digits; Число, которое необходимо озвучить
 * @return string; Последовательность аудиофайлов
 * @noinspection PhpUndefinedVariableInspection
 */
function SayCurrency($digits)
{
    extract(Parse($digits));

    $say = SayX00($millions)
        .SayX0($millions)
        .SayX($millions, ['millions.alaw', 'millions_n.alaw', 'millions_f.alaw'])
        .SayX00($thousand)
        .SayX0($thousand)
        .SayXforThousand($thousand, ['thousand.alaw', 'thousand_n.alaw', 'thousand_f.alaw'])
        .SayX00($hundreds)
        .SayX0($hundreds)
        .SayX($hundreds, ['rub.alaw', 'rub_n.alaw', 'rub_f.alaw'])
        .SayX0($units)
        .SayX($units, ['cop.alaw', 'cop_n.alaw', 'cop_f.alaw']);
    return trim(empty($say) ? '0.alaw rub_f.alaw pause.alaw' : $say);
}

/**
 * Выполнение команды
 *
 * @param $command; Команда
 * @return array; Результат выполнения
 */
function execute_agi($command)
{
    fwrite(STDOUT, "$command\n");
    fflush(STDOUT);
    $result = trim(fgets(STDIN));

    $ret = array('code' => -1, 'result' => -1, 'timeout' => false, 'data' => '');

    if (preg_match('/^([0-9]{1,3}) (.*)/', $result, $matches)) {
        $ret['code'] = $matches[1];
        $ret['result'] = 0;

        if (preg_match('/^result=([0-9a-zA-Z]*)\s?(?:\(?(.*?)\)?)?$/', $matches[2], $match)) {
            $ret['result'] = $match[1];
            $ret['timeout'] = $match[2] === 'timeout';
            $ret['data'] = $match[2];
        }
    }

    return $ret;
}

/**
 * Логирование
 *
 * @param $entry; Что вывести в лог
 * @param int $level; Уровень лога
 * @noinspection PhpUnused
 */
function log_agi($entry, $level = 1) {
    if (!is_numeric($level)) {
        $level = 1;
    }

    execute_agi("VERBOSE \"{$entry}\" {$level}");
}

/**
 * Разбор параметров
 */
$agivars = array();

while (!feof(STDIN)) {
    $agivar = trim(fgets(STDIN));

    if ($agivar === '') {
        break;
    } else {
        $agivar = explode(':', $agivar);
        $agivars[$agivar[0]] = trim($agivar[1]);
    }
}

extract($agivars);

/**
 * Директория хранения аудиофайлов
 */
const ALAW_PATH = '/var/lib/asterisk/sounds/custom/digits';

/**
 * Необходимое представление
 */
// Оставить копейки
//$digits = $argv[1];

// Отбрасывает копейки
$digits = intval($argv[1]);

// Округление копеек в большую сторону до рублей
//$digits = round($argv[1], 0, PHP_ROUND_HALF_UP);

/**
 * Если второй параметр при вызове 'currency', то число озвучивается как денежное представление.
 * В противном случае число озвучивается как есть.
 */
$result = $argv[2] == "'currency'" ? SayCurrency($digits) : SayNumber($digits);
$result = explode(' ', $result);

/**
 * Исполнение команд озвучивания набора аудиофайлов
 */
foreach($result as $item) {
    $command = 'STREAM FILE '.ALAW_PATH."/{$item} \"\"\n";
    execute_agi($command);
    sleep(1);
}

?>
