# SayDigits Озвучивание номеров и денежного представления в Asterisk

## Инсталляция
1. Положить файл say_digits.php в директорию /var/lib/asterisk/agi-bin.
2. Положить звуковые файлы в директорию /var/lib/asterisk/sounds/custom/digits.

## Использование
```
same => n,AGI(say_digits.php,${ClientID},'dogovor')
same => n,AGI(say_digits.php,${ClientAmount},'currency')
```
